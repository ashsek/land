package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
	"strings"
	//"os/exec"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

var peoples map[string]string

// Define the Khasra structure, with 2 properties.  Structure tags are used by encoding/json library
type Khasra struct {
	Circlerate int `json:"circlerate"`
	Area int `json:"area"`
	People map[string]string `json:"people"`
	Timeupdate string `json:"Timeupdate"`
}
/*
 * The Init method is called when the Smart Contract "registery" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 package main
*/

func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "registery"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */


 // Make a transaction s.t  checks valid owner, Hash and then transacts,

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryKhasra" {
		return s.queryKhasra(APIstub, args)
	 } else if function == "initLedger" {
	 	return s.initLedger(APIstub)
	 } else if function == "addKhasra" {
		return s.addKhasra(APIstub, args)
	} else if function == "queryAllKhasras" {
		return s.queryAllKhasras(APIstub)
	} else if function == "getHistoryForKhasra" { //get history of values for a marble
		return s.getHistoryForKhasra(APIstub, args)
	} else if function == "addPeople" {
		return s.addPeople(APIstub, args)
	} else if function == "trans" {
		return s.trans(APIstub, args)
	} else if function == "updt" {
		return s.updt(APIstub, args)
	} else if function == "rvn" {
		return s.rvn(APIstub, args)
	}
	// } else if function == "vfy" {
	// 	return s.vfy(APIstub, args)
	// }

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) trans(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	//Basic 3 arguments are Key, Owner , hash (To be deleted)
	if len(args) < 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3 or more than 3.")
	}

	var halen int = 1 // Define the Length of hash.
/*	
	if val, ok := m["Bell Labs"]; ok {
    	fmt.Println("Yolo")
		fmt.Println(val)
	}
	*/

	khasraAsBytes, _ := APIstub.GetState(args[0])
	khasra := Khasra{}

	json.Unmarshal(khasraAsBytes, &khasra)

	if !(strings.Contains(khasra.People[args[1]], args[2])) || len(khasra.People[args[1]]) == 0 {
		return shim.Error("Either invalid hash/owner pair or the hash dosent exist please update hash." + khasra.People[args[1]] + "    .." + args[2])
	}  else if strings.Contains(khasra.People[args[1]], args[2]){
		
		if len(khasra.People[args[1]])<= halen{
			delete(khasra.People, args[1]);
		} else if len(khasra.People[args[1]]) > halen{
			khasra.People[args[1]] = strings.Replace(khasra.People[args[1]], args[2], "", -1)
		}

		if len(args) > 3 {
			for i := 3; i < len(args)-1; i += 2 {
				if val, ok := khasra.People[args[i]]; ok {
					_ = val
    				if len(khasra.People[args[i]]) == 0{
    					return shim.Error("Please input the previous Hash.")
    				} else {
    					khasra.People[args[i]] = khasra.People[args[i]] + args[i+1]
    				}
				}else {
				khasra.People[args[i]] = args[i+1]
			}
			}
		}
	}

	
	khasra.Timeupdate = time.Now().String()

	khasraAsBytes, _ = json.Marshal(khasra)
	APIstub.PutState(args[0], khasraAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) updt(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	//basic 4 arguments are Key, Y or N for chakrabandi ,circle rate , Area
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 3 or more than 3.")
	}
	khasraAsBytes, _ := APIstub.GetState(args[0])
	khasra := Khasra{}

	json.Unmarshal(khasraAsBytes, &khasra)

	arg2, err2 := strconv.Atoi(args[2])
	arg3, err3 := strconv.Atoi(args[3])
	if err2 != nil {
		//yolo
	}
	if err3 != nil {
		//yolo
	}

	if args[1] == "N" {
		khasra.Circlerate = arg2
		khasra.Area = arg3
	}
	if args[1] == "Y" {
		//Do something
	}
	
	khasra.Timeupdate = time.Now().String()

	khasraAsBytes, _ = json.Marshal(khasra)
	APIstub.PutState(args[0], khasraAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) rvn(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	// Only Argument as khasra no.
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 in the format Khasra0")
	}

	khasraAsBytes, _ := APIstub.GetState(args[0])
	khasra := Khasra{}
	json.Unmarshal(khasraAsBytes, &khasra)

	yo := strconv.Itoa(khasra.Circlerate)
	yo2 := strconv.Itoa(khasra.Area)
 	z := "Circlerate: " + yo + "\n" + "Area: " + yo2
	return shim.Success([]byte(z))
}

func (s *SmartContract) queryKhasra(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	// Enter value as key for which to get query.
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 in the format Khasra0")
	}

	khasraAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(khasraAsBytes)
}

func (s *SmartContract) getHistoryForKhasra(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	khasraName := args[0]

	fmt.Printf("- start getHistoryForKhasra: %s\n", khasraName)

	resultsIterator, err := APIstub.GetHistoryForKey(khasraName)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing historic values for the marble
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		// if it was a delete operation on given key, then we need to set the
		//corresponding value null. Else, we will write the response.Value
		//as-is (as the Value itself a JSON marble)
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		// buffer.WriteString(", \"Timestamp\":")
		// buffer.WriteString("\"")
		// buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		// buffer.WriteString("\"")

		// buffer.WriteString(", \"IsDelete\":")
		// buffer.WriteString("\"")
		// buffer.WriteString(strconv.FormatBool(response.IsDelete))
		// buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForKhasra returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}


func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	Khasras := []Khasra{
		Khasra{Circlerate: 10000,	Area: 123, People: map[string]string{"jane": "somehash"}, Timeupdate: "Sample Time"},
	}

	i := 0
	for i < len(Khasras) {
		fmt.Println("i is ", i)
		khasraAsBytes, _ := json.Marshal(Khasras[i])
		APIstub.PutState("Khasra"+strconv.Itoa(i), khasraAsBytes)
		fmt.Println("Added", Khasras[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) addKhasra(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	arg1, err1 := strconv.Atoi(args[1])
	arg2, err2 := strconv.Atoi(args[2])
	if err1 != nil {
		//yolo
	}
	if err2 != nil {
		//yolo
	}

	var khasra = Khasra{Circlerate: arg1, Area: arg2, People: map[string]string{args[3]: args[4]}, Timeupdate: time.Now().String()}

	khasraAsBytes, _ := json.Marshal(khasra)
	APIstub.PutState(args[0], khasraAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) addPeople(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	khasraAsBytes, _ := APIstub.GetState(args[0])
	khasra := Khasra{}

	json.Unmarshal(khasraAsBytes, &khasra)
	khasra.People[args[1]] = args[2]
	khasra.Timeupdate = time.Now().String()

	khasraAsBytes, _ = json.Marshal(khasra)
	APIstub.PutState(args[0], khasraAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllKhasras(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "Khasra0"
	endKey := "Khasra999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllKhasras:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

// <<<<<<< HEAD
// func (s *SmartContract) vfy(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

// //	khasraAsBytes, _ := APIstub.GetState(args[0])

// 	//return shim.Success(khasraAsBytes)
// 	 //var l string = "from land import *;print a.validate_proof(" + a.get_proof(1) + "," + "'" + 36e0fd847d927d68475f32a94efff30812ee3ce87c7752973f4dd7476aa2e97e + "'" + ",'" + b8b1f39aa2e3fc2dde37f3df04e829f514fb98369b522bfb35c663befa896766 + ""
// 	cmd := exec.Command("python", "-c" ,"import os; os.system(\"pwd\")")//"from land import *;print a.validate_proof(a.get_proof(1),'36e0fd847d927d68475f32a94efff30812ee3ce87c7752973f4dd7476aa2e97e','b8b1f39aa2e3fc2dde37f3df04e829f514fb98369b522bfb35c663befa896766')")
// =======
// func (s *SmartContract) verify(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
//       for i< len(khasras) {
 
// 	if (len(args) != 1 && khasra.People[args[i]]==key){
	
// 	return shim.Error("Enter the key value to check khasra")
// 	fmt.printf("Found in the Khasra %s",khasraName )	
// 	}

// 	khasraAsBytes, _ := APIstub.GetState(args[i])
// 	return shim.Success(khasraAsBytes)
// 	i=i+1	

//       }
	
// 	  cmd := exec.Command("python", "-c" ,"from land import *;print a.validate_proof(a.get_proof					     (1),'36e0fd847d927d68475f32a94efff30812ee3ce87c7752973f4dd7476aa2e97e','b8b1f39aa2e3fc2dde37f3df04e829f514fb98369b522bfb35c663befa896766')")
// >>>>>>> d79448524b64a2d9fea5dca8912c8c3252cc37dc
//                 //cmd := exec.Command("python","-c","print \"Hello\"")
//                 // out, err := cmd.Output()
//                 // fmt.Printf("%s", out)
//                 // fmt.Printf("%s", err)
// <<<<<<< HEAD
//     output, err := cmd.CombinedOutput()
// 	if err != nil {
// 		l := fmt.Sprint(err) + ": " + string(output)
//     return shim.Error(l)
// 	} else {
//     return shim.Success([]byte(string(output)))
// 	}
// =======
//                 output, err := cmd.CombinedOutput()
// 	if err != nil 
// 	{
//     fmt.Println(fmt.Sprint(err) + ": " + string(output))
//     return
// 	} 	
// 	else {
//     fmt.Println(string(output))
// 	}
	
	
	
// >>>>>>> d79448524b64a2d9fea5dca8912c8c3252cc37dc
// }

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
