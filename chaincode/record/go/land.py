import hashlib
import binascii
import sys


class Land(object):
    def __init__(self, hash_type="sha256"):
        hash_type = hash_type.lower()
        if hash_type in ['sha256', 'md5']:
            self.hash_function = getattr(hashlib, hash_type)
        else:
            raise Exception('`hash_type` {} nor supported'.format(hash_type))

        self.reset_tree()

    def _to_hex(self, x):
        try:  
            return x.hex()
        except:  
            return binascii.hexlify(x)

    def reset_tree(self):
        self.leaves = list()
        self.levels = None
        self.is_ready = False

    def add_leaf(self, values, do_hash=False):
        self.is_ready = False
        if not isinstance(values, tuple) and not isinstance(values, list):
            values = [values]
        for v in values:
            if do_hash:
                v = v.encode('utf-8')
                v = self.hash_function(v).hexdigest()
            v = bytearray.fromhex(v)
            self.leaves.append(v)

    def get_leaf(self, index):
        return self._to_hex(self.leaves[index])

    def get_leaf_count(self):
        return len(self.leaves)

    def get_tree_ready_state(self):
        return self.is_ready

    def _calculate_next_level(self):
        solo_leave = None
        N = len(self.levels[0])  
        if N % 2 == 1:  
            solo_leave = self.levels[0][-1]
            N -= 1

        new_level = []
        for l, r in zip(self.levels[0][0:N:2], self.levels[0][1:N:2]):
            new_level.append(self.hash_function(l+r).digest())
        if solo_leave is not None:
            new_level.append(solo_leave)
        self.levels = [new_level, ] + self.levels  

    def make_tree(self):
        self.is_ready = False
        if self.get_leaf_count() > 0:
            self.levels = [self.leaves, ]
            while len(self.levels[0]) > 1:
                self._calculate_next_level()
        self.is_ready = True

    def get_merkle_root(self):
        if self.is_ready:
            if self.levels is not None:
                return self._to_hex(self.levels[0][0])
            else:
                return None
        else:
            return None

    def get_proof(self, index):
        if self.levels is None:
            return None
        elif not self.is_ready or index > len(self.leaves)-1 or index < 0:
            return None
        else:
            proof = []
            for x in range(len(self.levels) - 1, 0, -1):
                level_len = len(self.levels[x])
                if (index == level_len - 1) and (level_len % 2 == 1): 
                    index = int(index / 2.)
                    continue
                is_right_node = index % 2
                sibling_index = index - 1 if is_right_node else index + 1
                sibling_pos = "left" if is_right_node else "right"
                sibling_value = self._to_hex(self.levels[x][sibling_index])
                proof.append({sibling_pos: sibling_value})
                index = int(index / 2.)
            return proof

    def validate_proof(self, proof, token, merkle_root):
        merkle_root = bytearray.fromhex(merkle_root)
        token = bytearray.fromhex(token)
        if len(proof) == 0:
            return token == merkle_root
        else:
            proof_hash = token
            for p in proof:
                try:
                    
                    sibling = bytearray.fromhex(p['left'])
                    proof_hash = self.hash_function(sibling + proof_hash).digest()
                except:
                    sibling = bytearray.fromhex(p['right'])
                    proof_hash = self.hash_function(proof_hash + sibling).digest()
            return proof_hash == merkle_root

    def verify_proof(self,name,khasra_no):
        record = {"Rishav":1,"Ashwin":2,"Saubhagya":3,"A":4,"B":5,"C":6}
	if record[name] == khasra_no:		
                print("manual verification needed")
		

	else:
 		print ("reject")

a = Land(hash_type="md5")
hex_data = '05ae04314577b2783b4be98211d1b72476c59e9c413cfb2afa2f0c68e0d93911'
list_data = ['a', 'b']

a.add_leaf(hex_data)
a.add_leaf(list_data, True)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
a.add_leaf(hex_data)
leaf_count =  a.get_leaf_count();
#print(leaf_count)        #should be 16 leaf nodes
leaf_value =  a.get_leaf(1)
#print(leaf_value)

a.make_tree();
is_ready = a.is_ready
#print(is_ready)

root_value = a.get_merkle_root(); 
#print(root_value)

proof = a.get_proof(1)
#print(proof)
token = '36e0fd847d927d68475f32a94efff30812ee3ce87c7752973f4dd7476aa2e97e'
merkle_root = 'b8b1f39aa2e3fc2dde37f3df04e829f514fb98369b522bfb35c663befa896766'
is_valid = a.validate_proof(proof, token, merkle_root)
#print(is_valid)
 

       
