package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

// Define the hash structure, with 2 properties.  Structure tags are used by encoding/json library
type R_Hash struct {
	District string `json:"district"`
	Tehsil string `json:"tehsil"`
	Location string `json:"location"`
	Hash  string `json:"hash"`
	TimeCommit string `json: "Timecommit"`
	
}

/*
 * The Init method is called when the Smart Contract "registery" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "registery"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryHash" {
		return s.queryHash(APIstub, args)
	 } else if function == "initLedger" {
	 	return s.initLedger(APIstub)
	 } else if function == "addHash" {
		return s.addHash(APIstub, args)
	} else if function == "queryAllHashes" {
		return s.queryAllHashes(APIstub)
	} else if function == "changeHash" {
		return s.changeHash(APIstub, args)
	} else if function == "getHistoryForHash" { //get history of values for a marble
		return s.getHistoryForHash(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) queryHash(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	hashAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(hashAsBytes)
}

func (s *SmartContract) getHistoryForHash(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	hashName := args[0]

	fmt.Printf("- start getHistoryForHash: %s\n", hashName)

	resultsIterator, err := APIstub.GetHistoryForKey(hashName)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing historic values for the marble
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		// if it was a delete operation on given key, then we need to set the
		//corresponding value null. Else, we will write the response.Value
		//as-is (as the Value itself a JSON marble)
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		// buffer.WriteString(", \"Timestamp\":")
		// buffer.WriteString("\"")
		// buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		// buffer.WriteString("\"")

		// buffer.WriteString(", \"IsDelete\":")
		// buffer.WriteString("\"")
		// buffer.WriteString(strconv.FormatBool(response.IsDelete))
		// buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForHash returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}


func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	Hashes := []R_Hash{
		R_Hash{District: "01",	Hash: "7cb6fa91c124913f7a75e3153339234f", TimeCommit: "Sample Time"},
	}

	i := 0
	for i < len(Hashes) {
		fmt.Println("i is ", i)
		hashAsBytes, _ := json.Marshal(Hashes[i])
		APIstub.PutState("R_Hash"+strconv.Itoa(i), hashAsBytes)
		fmt.Println("Added", Hashes[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) addHash(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	var hash = R_Hash{District: args[1], Tehsil: args[2], Location: args[3], Hash: args[4], TimeCommit: time.Now().String()}

	hashAsBytes, _ := json.Marshal(hash)
	APIstub.PutState(args[0], hashAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllHashes(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "R_Hash0"
	endKey := "R_Hash999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllHashes:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) changeHash(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	hashAsBytes, _ := APIstub.GetState(args[0])
	hash := R_Hash{}

	json.Unmarshal(hashAsBytes, &hash)
	hash.Hash = args[1]
	hash.TimeCommit = args[2]

	hashAsBytes, _ = json.Marshal(hash)
	APIstub.PutState(args[0], hashAsBytes)

	return shim.Success(nil)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
